FROM python:3.7
MAINTAINER Alexander Yanzin <AlexanderYanzin@hotmail.com>
COPY requirements.txt /tmp
RUN pip3 install -r /tmp/requirements.txt; mkdir /code
WORKDIR /code
EXPOSE 8080
COPY ./src/server/ /code/
CMD ["/usr/local/bin/python3.7", "__main__.py"]