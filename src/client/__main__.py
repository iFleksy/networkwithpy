import json
import os
import socket
import argparse
import select
import zlib
import yaml
from datetime import datetime

# Константы настроек по умолчанию
DEFAULT_SETTINGS = {
    "host": "127.0.0.1",
    "port": 8080,
    "encoding": 'utf-8'
}


def args_parse():
    """
    Функция для парсинга аргументов
    :return: args
    """
    args = argparse.ArgumentParser('Connection options')
    args.add_argument('-H', '--host', type=str, help='Chat server host', required=True)
    args.add_argument('-p', '--port', type=int, help='Chat server port', required=True)
    args.add_argument('-m', '--mode', type=str, help='type working client', default='r')
    args.add_argument('-c', '--config', type=str, help='Path to config file', default='config.yaml')
    return args.parse_args()


def get_configs_from_file(file_path: str) -> dict:
    config = DEFAULT_SETTINGS
    if os.path.exists(file_path):
        with open(file_path, 'r') as config_file:
            try:
                config = yaml.load(config_file, Loader=yaml.Loader).get('server', DEFAULT_SETTINGS)
            except Exception:
                pass
    return config


def main():
    # Получаем агрументы из командной строки
    args = args_parse()

    # Получаем конфигурацию из файла или дефолтные настройки
    config = get_configs_from_file(args.config)

    # Определяем стандартные настройки
    host = args.host
    port = args.port
    encoding = config['encoding']
    buffer_size = config['buffer_size']

    # Иницилизируем сокет. подключемся к серверу
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        sock.connect((host, port))
    except Exception as err:
        print(f"Cannot connect to server '{host}:{port}'. Error: '{err}'")
        exit(1)

    # Проверяем режим работы клиента
    if args.mode != 'r':
        while True:
            action = input("Введите действие: ")
            message = input("Введите сообщение: ")
            request = {
                "action": action,
                "user": 'anon',
                "data": {'message': message},
                "time": datetime.now().timestamp()
            }
            compressed_request = zlib.compress(
                json.dumps(request).encode(encoding)
            )
            sock.send(compressed_request)
    else:
        while True:
            # Проверяем готовность сокетов на считывание информации
            read_list, write_list, probleb_list = select.select(
                [], [sock], [], 0
            )

            for connect in write_list:
                data = connect.recv(1024)
                b_response = zlib.decompress(data)

                print(json.dumps(json.loads(b_response.decode(encoding)), indent=4))


if __name__ == "__main__":
    main()
