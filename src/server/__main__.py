import json
import os
import logging
import select
from socket import socket, AF_INET, SOCK_STREAM
from utils import load_logger
from server.handlers import handle_default_request
from settings import (
    SERVER_HOST, SERVER_PORT,
    SERVER_MAX_CONNECTION, SERVER_BUFFER
)


def main():
    # Соединения клиентов
    connections = []

    # Запросы клиентов
    requests = []

    # Иницилизация логгера
    load_logger()
    log = logging.getLogger('server.main')
    sock = socket(AF_INET, SOCK_STREAM)
    server_host = os.getenv('SERVER_HOST', SERVER_HOST)
    server_port = int(os.getenv('SERVER_PORT', SERVER_PORT))
    try:
        sock.bind((server_host, server_port))
        sock.settimeout(0)
        sock.listen(SERVER_MAX_CONNECTION)
        log.info(f"Server started with { server_host }:{ server_port }")
        while True:
            try:
                client, addr = sock.accept()
                connections.append(client)
                log.info(f"Client detected { addr }")
            except BlockingIOError:
                pass

            r_list, w_list, err_list = select.select(
                connections, connections, connections, 0
            )

            for r_client in r_list:
                try:
                    b_requests = r_client.recv(SERVER_BUFFER)
                except Exception as err:
                    r_client.close()
                    continue
                requests.append(b_requests)

            if requests:
                b_request = requests.pop()
                b_response = handle_default_request(b_request)

                for w_client in w_list:
                    try:
                        w_client.send(b_response)
                    except Exception as err:
                        w_client.close()
                        continue

    except KeyboardInterrupt:
        log.info('Client closed')
        sock.close()
    except Exception as err:
        sock.close()
        raise err


if __name__ == "__main__":
    main()
