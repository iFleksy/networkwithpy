from datetime import datetime

BAD_REQUEST = 400
NOT_FOUND = 404
INTERNAL_ERROR = 500
SUCCESS = 200
PERMISSION_DENIED = 403


def validate_request(request: dict) -> bool:
    request_time = request.get('time', None)
    request_action = request.get('action', None)

    if request_action and request_time:
        return True

    return False


def make_response(request: dict, code: int, data=None) -> dict:
    return {
        'action': request.get('action', 'undefined'),
        'user': request.get('user', 'unknown'),
        'time': datetime.now().timestamp(),
        'data': data,
        'code': code
    }


def response_400(request):
    return make_response(request, BAD_REQUEST, 'Wrong request format')


def response_404(request):
    return make_response(request, NOT_FOUND, 'Action is not supported')


def response_500(request):
    return make_response(request, INTERNAL_ERROR, 'Internal error')


def response_403(request):
    return make_response(request, PERMISSION_DENIED, 'Access denied')
