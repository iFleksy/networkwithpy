import inspect
from functools import wraps
import logging
import zlib
from protocol import response_403

log = logging.getLogger('decoration')


def log_func(attr_name):
    def deco(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            info = inspect.stack()[1]
            if hasattr(func, attr_name):
                print(getattr(func, attr_name))
            log.debug("Function: '{}' called from function '{}'".format(func.__name__, info.function))
            return func(*args, **kwargs)
        return wrapper
    return deco


def login_required(func):
    def wrapper(request, *args, **kwargs):
        user = request.get('user', None)
        if user is None:
            return response_403(request)

        return func(request, *args, **kwargs)

    return wrapper


def compressed(func):
    def wrapper(request, *args, **kwargs):
        if request:
            b_request = zlib.decompress(request)
            response = func(b_request, *args, **kwargs)
            return zlib.compress(response)
        return func(request, *args, **kwargs)

    return wrapper
