from .controllers import get_exception

routes = [
    {
        'action': 'echo',
        'controller': get_exception
    }
]
