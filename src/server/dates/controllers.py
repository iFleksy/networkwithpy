from protocol import make_response, SUCCESS
from datetime import datetime
from decorations import login_required


@login_required
def get_datetime_now(request: dict) -> dict:
    return make_response(request, SUCCESS, {'date': datetime.now().strftime('%Y.%m.%d')})
