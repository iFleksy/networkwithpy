from .controllers import (
    get_datetime_now
)

routes = [
    {
        'action': 'now',
        'controller': get_datetime_now
    }
]
