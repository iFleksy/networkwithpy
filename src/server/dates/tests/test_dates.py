import unittest
from dates.controllers import get_datetime_now
from protocol import SUCCESS
from datetime import datetime


class TestDateTimeNow(unittest.TestCase):
    def test_001_get_datetime_now(self):
        request = {}
        response = get_datetime_now(request)

        self.assertEqual(SUCCESS, response.get('code', 0), "Echo server return code '{}' but expected '{}'".format(
            response.get('code', 0), SUCCESS
        ))

        self.assertIs(type(response.get('data')), dict, "Data from response have type '{}' but expected '{}'".format(
            type(response.get('data')), 'dict'
        ))

        self.assertEqual(datetime.now().strftime('%Y.%m.%d'), response['data'].get('date', 'undefined'))
