import logging
import logging.config
import settings
import sys

LOGGING_DEFAULT = {
    'version': 1,
    'formatters': {
        'standard': {
            'format': '[%(asctime)s][%(levelname)s][%(name)s]: %(message)s'
        },
        'decor_format': {
            'format': '%(asctime)s: %(message)s'
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
            'stream': sys.stdout
        },
        'console_decor': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'decor_format',
            'stream': sys.stdout
        },
        'fileWithRotate': {
            'class': 'logging.handlers.RotatingFileHandler',
            'level': 'DEBUG',
            'formatter': 'standard',
            'filename': '/tmp/server_all.log',
            'maxBytes': 1024,
            'backupCount': 3
        },
        'fileError': {
            'class': 'logging.FileHandler',
            'filename': '/tmp/server_error.log',
            'mode': 'w',
            'level': 'ERROR',
            'formatter': 'standard',
        }
    },
    'loggers': {
        'server': {
            'level': 'DEBUG',
            'handlers': ['console', 'fileWithRotate', 'fileError'],
        },
        'decoration': {
            'level': 'DEBUG',
            'handlers': ['console_decor']
        }
    }
}


def load_logger():
    if hasattr(settings, 'LOGGING'):
        try:
            logging.config.dictConfig(settings.LOGGING)
        except Exception:
            logging.config.dictConfig(LOGGING_DEFAULT)
    else:
        logging.config.dictConfig(LOGGING_DEFAULT)


load_logger()
