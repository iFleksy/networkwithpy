from routes import resolve
import logging
import json
from .settings import ENCODING
from protocol import validate_request, response_400, response_500, response_404
from .decorations import compressed


@compressed
def handle_default_request(raw_request: bytes) -> str:
    log = logging.getLogger(__name__)
    try:
        request = json.loads(
            raw_request.decode(ENCODING)
        )
    except Exception as err:
        log.error("Cannot json load object. Error: '{}'".format(err))
        request = {}
    if validate_request(request):
        log.debug(f'Validate request from user success.')
        action = request.get('action', None)
        controller = resolve(action)
        if controller:
            try:
                response = controller(request)
            except Exception as error:
                response = response_500(request)
        else:
            log.error("Action '{}' is not defined".format(action))
            response = response_404(request)
    else:
        log.error("Request is not valid")
        response = response_400(request)
    return json.dumps(response).encode(ENCODING)
