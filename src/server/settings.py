import os
import yaml

# Получаем каталог в котором находимся
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
SERVER_HOST = '0.0.0.0'
SERVER_PORT = 8001
ENCODING = 'utf-8'
SERVER_MAX_CONNECTION = 5
SERVER_BUFFER = 1024

INSTALLED_MODULES = [
    'dates',
    'echo',
    'exception'
]
