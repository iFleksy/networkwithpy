from functools import reduce
from settings import INSTALLED_MODULES
from importlib import __import__


def get_server_routes():
    return [x[0] for x in reduce(
        lambda value, item: value + [getattr(item, 'routes', [])],
        reduce(
            lambda value, item: value + [getattr(item, 'routes', [])],
            reduce(
                lambda value, item: value + [__import__(f'{ item }.routes')],
                INSTALLED_MODULES,
                []
            ),
            []
        ),
        []
    )]


def resolve(action, routes=None):
    routes_mapping = {
        route['action']: route['controller']
        for route in routes or get_server_routes()
    }
    return routes_mapping.get(action, None)
