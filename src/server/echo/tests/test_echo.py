import unittest
from echo.controllers import get_echo
from protocol import SUCCESS, BAD_REQUEST


class TestGetEcho(unittest.TestCase):
    def test_001_get_echo(self):
        request = {
            "action": "echo",
            "data": "Hello, it's testing function"
        }
        response = get_echo(request)
        self.assertEqual(SUCCESS, response.get('code', 0), "Echo server return code '{}' but expected '{}'".format(
            response.get('code', 0), SUCCESS
        ))
        self.assertIs(type(response.get('data')), dict, "Data from response have type '{}' but expected '{}'".format(
            type(response.get('data')), 'dict'
        ))
        self.assertEqual(request['data'], response['data'].get('message'))

    def test_002_get_echo_bad_data(self):
        request = {}
        response = get_echo(request)
        self.assertEqual(BAD_REQUEST, response.get('code', 0), "Echo server return code '{}' but expected '{}'".format(
            response.get('code', 0), BAD_REQUEST
        ))
