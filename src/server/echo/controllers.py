from protocol import make_response, SUCCESS, response_400


def get_echo(request):
    data = request.get('data')
    if data:
        return make_response(request, SUCCESS, {'message': data})

    return response_400(request)
