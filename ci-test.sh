#!/usr/bin/env bash
BASE_DIR=`pwd`
PYTHONPATH=`pwd`

PROJECTDIR=`echo $0 | sed 's/ci-test.sh//g'`
cd ${PROJECTDIR}
SERVER_DIR=src/server
CLIENT_DIR=src/client
PY_TEST_CONFIG=${PROJECTDIR}/pytest.cfg
PYTHONPATH=${PYTHONPATH}:${PROJECTDIR}/${SERVER_DIR}:${PROJECTDIR}/${CLIENT_DIR} py.test -c ${PY_TEST_CONFIG} --pep8 -v
