import sys
import time
import subprocess
"""
Task 1
    Каждое из слов «разработка», «сокет», «декоратор» представить в строковом формате и
    проверить тип и содержание соответствующих переменных. Затем с помощью
    онлайн-конвертера преобразовать строковые представление в формат Unicode и также
    проверить тип и содержимое переменных.
"""

print('\n', '*'*15, 'Task 1', '*'*15)

words = ['разработка', 'сокет', 'декоратор']

for index, word in enumerate(words):
    print("Word # {}. Type: '{}', Value: '{}', Bytes: '{}'".format(
        index + 1, type(word), word, word.encode('utf-8')
    ))

"""
Task 2
    Каждое из слов «class», «function», «method» записать в байтовом типе без преобразования в 
    последовательность кодов (не используя методы encode и decode) и определить тип, 
    содержимое и длину соответствующих переменных.
"""
print('\n', '*'*15, 'Task 2', '*'*15)

words = [b'class', b'function', b'method']

for index, word in enumerate(words):
    print("Word # {}. Type: '{}', Value: '{}', Len: '{}'".format(
        index + 1, type(word), word, len(word)
    ))

"""
Task 3
    Определить, какие из слов «attribute», «класс», «функция», «type» невозможно записать в
    байтовом типе
"""
print('\n', '*'*15, 'Task 3', '*'*15)

byte_word = b'attribute'
# byte_word2 = b'класс'
# byte_word3 = b'функция'

# Поспим. быстро пишится в stderr
time.sleep(2)
print('byte_word2 = b\'класс\'\n^\n\t\tSyntaxError: bytes can only contain ASCII literal characters.")',
      file=sys.stderr)
time.sleep(2)
"""
Task 4
    Преобразовать слова «разработка», «администрирование», «protocol», «standard» из
    строкового представления в байтовое и выполнить обратное преобразование (используя
    методы encode и decode).
"""
print('\n', '*'*15, 'Task 4', '*'*15)

words = ['разработка', 'администрирование', 'protocol', 'standard']

for index, word in enumerate(words):
    print("\nWord # {}".format(index + 1))
    print("Type: '{}', Value: '{}'.".format(type(word), word))
    byte_word = word.encode('utf-8')
    print("Type: '{}', Value: '{}'.".format(type(byte_word), byte_word))

"""
Task 5
    Выполнить пинг веб-ресурсов yandex.ru, youtube.com и преобразовать результаты из
    байтовового в строковый тип на кириллице
"""
print('\n', '*'*15, 'Task 5', '*'*15)


def ping_resource(resource: str, limit: str = '3') -> None:
    """
    Writing to stdout result work cmd ping
    :param resource -> resource to ping
    :param limit -> Max count ping resource.
    :return: None
    """
    if not isinstance(resource, str):
        raise TypeError("Arg 'resource' must be type str")
    request = subprocess.Popen(['ping', resource, '-c', limit], stdout=subprocess.PIPE)
    for line in request.stdout:
        print(line.decode('utf-8'))


resources = ['yandex.ru', 'youtube.com']

for res in resources:
    print("\n\nStart ping resource: '{}'".format(res))
    ping_resource(res)


"""
Task 6
    Создать текстовый файл test_file.txt, заполнить его тремя строками: «сетевое
    программирование», «сокет», «декоратор». Проверить кодировку файла по умолчанию.
    Принудительно открыть файл в формате Unicode и вывести его содержимое.
"""
print('\n', '*'*15, 'Task 6', '*'*15)

file = open('test_file.txt', 'r', encoding='utf-8')
print("Info about handle: {}".format(file))


print("Out data from file 'test_file.txt'")
for file_line in file.readlines():
    print(file_line, end='')

file.close()
