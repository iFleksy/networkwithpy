import os
import re
import yaml
import json
import csv
from datetime import datetime


# Получаем каталог в котором находимся
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
# Константа имени папки с датой для работы
DATA_DIR = 'data'
# Формируем полный путь до файла
FULL_DATA_PATH = os.sep.join([BASE_DIR, DATA_DIR])

"""
Task 1
    1. Создать функцию get_data(), в которой в цикле осуществляется перебор файлов с данными,
    их открытие и считывание данных. В этой функции из считанных данных необходимо с помощью
    регулярных выражений извлечь значения параметров «Изготовитель системы», «Название ОС», «Код продукта»,
    «Тип системы». Значения каждого параметра поместить в соответствующий список.
    Должно получиться четыре списка — например, os_prod_list, os_name_list, os_code_list, os_type_list.
    В этой же функции создать главный список для хранения данных отчета — например, main_data — и поместить в него
    названия столбцов отчета в виде списка: «Изготовитель системы», «Название ОС», «Код продукта», «Тип системы».
    Значения для этих столбцов также оформить в виде списка и поместить в файл main_data (также для каждого файла);
    
    2. Создать функцию write_to_csv(), в которую передавать ссылку на CSV-файл.
    В этой функции реализовать получение данных через вызов функции get_data(), а также сохранение подготовленных
    данных в соответствующий CSV-файл; Проверить работу программы через вызов функции write_to_csv().
"""
print("{border} Task 1 {border}".format(border="*"*15))

# Null char
NULL_CHAR = "-"

# Main DATA FILE
MAIN_DATA_FILE = 'main_data.csv'

# Определяем регулярные выражения
prod_system_pattern = re.compile(r'(Изготовитель системы:\s*)(.*)')
name_os_pattern = re.compile(r'(Название ОС:\s*)(.*)')
product_code_pattern = re.compile(r'(Код продукта:\s*)(.*)')
type_system_pattern = re.compile(r'(Тип системы:\s*)(.*)')


def get_data() -> list:
    """
    Parse file and returning data about system's
    :return: Array with information's about systems
    """

    # Список файлов для выборки данных
    data_files = ['info_1.txt', 'info_2.txt', 'info_3.txt']

    os_prod_list = list()
    os_name_list = list()
    os_code_list = list()
    os_type_list = list()
    main_data = [
        ['Изготовитель системы', 'Название ОС', 'Код продукта', 'Тип системы']
    ]

    for file_name in data_files:
        full_file_path = os.sep.join([FULL_DATA_PATH, file_name])
        # Проверяем существование файла
        if not os.path.exists(full_file_path):
            raise Exception("Not exist file: '{}'".format(full_file_path))

        if not os.path.isfile(full_file_path):
            raise Exception("Is not file in path: '{}'".format(full_file_path))

        with open(full_file_path, 'r', encoding='cp1251') as source_file:
            data = source_file.read()

        # Парсим полученно содержимое файла по патернам и раскладываем по спискам
        prod_system = prod_system_pattern.search(data)
        os_prod_list.append(prod_system.group(2).strip() if prod_system is not None else NULL_CHAR)

        name_os = name_os_pattern.search(data)
        os_name_list.append(name_os.group(2).strip() if name_os is not None else NULL_CHAR)

        code = product_code_pattern.search(data)
        os_code_list.append(code.group(2).strip() if code is not None else NULL_CHAR)

        type_system = type_system_pattern.search(data)
        os_type_list.append(type_system.group(2).strip() if code is not None else NULL_CHAR)

    # Формируем mail data из полученных значений
    for index in range(0, len(os_code_list)):
        main_data.append(
            [os_prod_list[index], os_name_list[index], os_code_list[index], os_type_list[index]]
        )

    return main_data


def write_to_csv(file_to_write) -> None:
    """
    Get data from function get_data and write in to file with format csv
    :param file_to_write: IOWrapper file to save
    :return: None
    """
    writer = csv.writer(file_to_write)
    data = get_data()
    writer.writerows(data)


data_file = os.sep.join([FULL_DATA_PATH, MAIN_DATA_FILE])
with open(data_file, 'w') as file:
    write_to_csv(file)

print("Result working function write_to_csv:\n")
with open(data_file, 'r') as file:
    print(file.read())

"""
Task 2: Задание на закрепление знаний по модулю json. 
    Есть файл orders в формате JSON с информацией о заказах.
    Написать скрипт, автоматизирующий его заполнение данными. Для этого: Создать функцию write_order_to_json(), в
    которую передается 5 параметров — товар (item), количество (quantity), цена (price), покупатель (buyer),
    дата (date). Функция должна предусматривать запись данных в виде словаря в файл orders.json.
    При записи данных указать величину отступа в 4 пробельных символа;
    Проверить работу программы через вызов функции write_order_to_json() с передачей в нее значений каждого параметра.
"""
print("\n{border} Task 2 {border}".format(border="*"*15))
ORDER_FILE = 'orders.json'
FULL_ORDER_FILE_PATH = os.sep.join([FULL_DATA_PATH, ORDER_FILE])


def clean_orders(file_path: str) -> None:
    # Проверяем существование файла
    if not os.path.exists(file_path):
        raise Exception("Not exist file: '{}'".format(file_path))

    if not os.path.isfile(file_path):
        raise Exception("Is not file in path: '{}'".format(file_path))

    # Создаем пустой список заказов
    clean_data = {
        "orders": []
    }

    # Обнуляем данные в файле
    with open(file_path, 'w') as source_file:
        json.dump(clean_data, source_file)


def write_order_to_json(file_path: str, item: str, quantity: int, price: float, buyer: str, date: str) -> None:
    if not os.path.exists(file_path):
        raise Exception("Not exist file: '{}'".format(file_path))

    if not os.path.isfile(file_path):
        raise Exception("Is not file in path: '{}'".format(file_path))

    order = {
        "item": item,
        "quantity": quantity,
        "price": price,
        "buyer": buyer,
        "date": date
    }

    # Получам старые данные
    with open(file_path, 'r') as source_file:
        data = json.load(source_file)

    # Если нет ключа 'orders' то получем пустой массив
    orders_list = data.get('orders', [])

    # Добавляем новый заказ к существующим
    orders_list.append(order)

    # Обновляем данных об заказах
    data['orders'] = orders_list

    # Сохраняем данные
    with open(file_path, 'w') as source_file:
        json.dump(data, source_file, indent=4)


clean_orders(FULL_ORDER_FILE_PATH)

# формируем список аргументов для проверки функции
insert_args = [
    {
        "item": 'Nvidia gtx 2080ti',
        "quantity": 1,
        "price": 89000.99,
        "buyer": 'awesomeMan',
        "date": datetime.now().strftime("%Y-%m-%d")
    },
    {
        "item": 'Coca-cola, 2',
        "quantity": 12,
        "price": 12 * 65.00,
        "buyer": 'Alex',
        "date": datetime(year=2018, month=12, day=25).strftime("%Y-%m-%d")
    }
]

# Проверяем функцию. должно быть 2 заказа
for args in insert_args:
    write_order_to_json(FULL_ORDER_FILE_PATH, **args)

# Уберем ключ orders из файла и ещё раз проверим
with open(FULL_ORDER_FILE_PATH, 'w') as file:
    file.write("{}")

for args in insert_args:
    write_order_to_json(FULL_ORDER_FILE_PATH, **args)

# Выводим результат в консоль
print("Result working function write_order_to_json:\n")
with open(FULL_ORDER_FILE_PATH, 'r') as file:
    print(file.read())


"""
Task 3: Задание на закрепление знаний по модулю yaml
    Написать скрипт, автоматизирующий сохранение данных в файле YAML-формата. Для этого: Подготовить данные
    для записи в виде словаря, в котором первому ключу соответствует список, второму — целое число, третьему — вложенный
    словарь, где значение каждого ключа — это целое число с юникод-символом, отсутствующим в кодировке ASCII
    (например, €); 
    
    Реализовать сохранение данных в файл формата YAML — например, в файл file.yaml. При этом обеспечить стилизацию
    файла с помощью параметра default_flow_style, а также установить возможность работы с юникодом:
    allow_unicode = True; Реализовать считывание данных из созданного файла и проверить, совпадают ли они с исходными.
"""
print("\n{border} Task 3 {border}".format(border="*"*15))
print("Result dump and load yaml to\\from file:\n")

YAML_FILE = 'file.yaml'
YAML_FILE_PATH = os.sep.join([FULL_DATA_PATH, YAML_FILE])

PREPARE_DATA = {
    "Item1": ['index1', 'index2', 'index3'],
    "Item2": 4,
    "Item3": {
        "Item3_1": "1€",
        "Item3_2": "2Ю"
    }
}

with open(YAML_FILE_PATH, 'w', encoding='utf-8') as file:
    yaml.dump(PREPARE_DATA, file, default_flow_style=True, allow_unicode=True)

with open(YAML_FILE_PATH, 'r', encoding='utf-8') as file:
    print(yaml.load(file, Loader=yaml.FullLoader))
    print(PREPARE_DATA)
